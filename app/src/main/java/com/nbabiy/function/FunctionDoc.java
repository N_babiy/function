package com.nbabiy.function;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import com.nbabiy.function.SimpleGestureFilter;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Developer on 01.02.2017.
 */

public class FunctionDoc extends AppCompatActivity {
    ImageView imageView;
    TextView pages;
    PhotoViewAttacher mPhotoViewAttacher;
    int x = 0;

    BitmapFactory.Options options = new BitmapFactory.Options();

    Bitmap bitmap;
    int [] drawable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.function_activity);

        pages = (TextView)findViewById(R.id.pages);

        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(this.getIntent().getStringExtra("title"));

        imageView = (ImageView)findViewById(R.id.image_view);

        Bundle b = this.getIntent().getExtras();
        drawable = b.getIntArray("array");

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inSampleSize = 4;

        bitmap = BitmapFactory.decodeResource(getResources(),drawable[0], options);
        imageView.setImageBitmap(bitmap);

        Button prev = (Button) findViewById(R.id.prev);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = null;
                if (x == 0 ){
                    x = drawable.length-1;
                }else{
                    x--;
                }
                mPhotoViewAttacher.setZoomable(false);
                setImage(drawable[x]);
                mPhotoViewAttacher.setZoomable(true);
                pages.setText((x+1)+"/"+drawable.length);
            }
        });

        Button next = (Button) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = null;
                if (x == drawable.length-1) {
                    x = 0;
                } else {
                    x++;
                }
                mPhotoViewAttacher.setZoomable(false);
                setImage(drawable[x]);
                mPhotoViewAttacher.setZoomable(true);
                pages.setText((x+1)+"/"+drawable.length);
            }
        });

        pages.setText((x+1)+"/"+drawable.length);

        mPhotoViewAttacher = new PhotoViewAttacher(imageView);

        mPhotoViewAttacher.update();
    }

    public void setImage(int x){
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inSampleSize = 4;
        bitmap = BitmapFactory.decodeResource(getResources(), x, options);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bitmap = null;
    }
}
