package com.nbabiy.function;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nbabiy.function.db.FunctionOpenHelper;
import com.nbabiy.function.model.TestActivityModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Developer on 25.02.2017.
 */

public class TestActivity extends AppCompatActivity {


    //db
    SQLiteDatabase db;
    Cursor cursor;
    Cursor cursorArray;
    //data
    List<TestActivityModel> all;
    List<TestActivityModel> data;
    int testNum;
    int count;
    int x [];
    int correctCount=0;

    boolean startActivity = true;
    boolean procesActivity = false;
    boolean endActivity = false;
    boolean seen = false;

    android.support.v7.app.ActionBar ab;

    TestActivityModel tam;
    //layouts
    RelativeLayout startLayout;
    RelativeLayout progressLayout;
    RelativeLayout endLayout;
    //Start test
    Spinner countTest;
    int countTests;
    Button startTest;
    //InProgress test
    TextView number;
    ImageView test_draw;
    HorizontalScrollView scrollView;
    LinearLayout buttons;
    ImageView a;
    ImageView b;
    ImageView v1;
    ImageView g;
    ImageView d;
    Button buttonA;
    Button buttonB;
    Button buttonV;
    Button buttonG;
    Button buttonD;
    TextView ans_string;
    TextView answer;
    TextView cor_string;
    TextView correct;
    Button prev;
    Button stop;
    Button next;
    //Result test
    TextView shapka_res;
    TextView result;
    Button exit;
    Button review;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.test_activity);

        //global
        all = new ArrayList<TestActivityModel>();
        data = new ArrayList<TestActivityModel>();

        ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(R.string.generalTest);

        //startActivity
        startLayout = (RelativeLayout) findViewById(R.id.start_test);
        progressLayout = (RelativeLayout) findViewById(R.id.test);
        endLayout = (RelativeLayout) findViewById(R.id.end_test);

        startLayout.setVisibility(View.VISIBLE);
        progressLayout.setVisibility(View.INVISIBLE);
        endLayout.setVisibility(View.INVISIBLE);

        countTest = (Spinner) findViewById(R.id.spinner);

        startTest = (Button) findViewById(R.id.start_button);
        startTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ab.setDisplayHomeAsUpEnabled(false);
                startActivity = false;
                switch (Integer.valueOf(countTest.getSelectedItem().toString())) {
                    case 5:
                        countTests = 5;
                        break;
                    case 10:
                        countTests = 10;
                        break;
                    case 15:
                        countTests = 15;
                        break;
                    case 20:
                        countTests = 20;
                        break;
                }
                System.out.println(countTests);

                SQLiteOpenHelper openHelper = new FunctionOpenHelper(getApplicationContext());
                db = openHelper.getReadableDatabase();

                cursor = db.rawQuery("Select * from TEST where topic = ?", new String[]{"Загальний Тест"});
                cursor.moveToFirst();
                cursorArray = db.rawQuery("Select * from TEST_ANS", null);
                cursorArray.moveToFirst();

                procesActivity = true;
                endActivity = false;

                for (int i = 0; i < cursor.getCount(); i++) {
                    TestActivityModel tam = new TestActivityModel(cursor.getInt(1), cursor.getInt(2), new int[]{},
                            0,
                            cursor.getInt(3),
                            false);
                    x = new int[5];
                    for (int j = 0; j < 5; j++){
                        x[j] = cursorArray.getInt(2);
                        cursorArray.moveToNext();
                    }
                    tam.setAnswers(x);
                    all.add(tam);
                    cursor.moveToNext();
                }

                data.add(all.get(random(0, all.size() - 1)));
                for(int i = 0; i < countTests-1; i++){
                    boolean b = true;
                    while (b) {
                        TestActivityModel t = all.get(random(0, all.size() - 1));


                        for (int j = 0; j < data.size(); j++) {

                            if (t.equals(data.get(j))) {
                                break;
                            }else if(j+1 == data.size()){
                                    data.add(t);
                                b = false;
                                break;
                            }
                        }
                    }
                }
                startLayout.setVisibility(View.INVISIBLE);
                progressLayout.setVisibility(View.VISIBLE);

                startTest.setVisibility(View.INVISIBLE);

                testNum = 0;
                count = data.size()-1;
                System.out.println(count);
                setTest(testNum);

            }
        });

        //InProgress test
        number = (TextView)findViewById(R.id.number);
        test_draw = (ImageView)findViewById(R.id.test_draw);
        scrollView = (HorizontalScrollView)findViewById(R.id.scrollView);
        a = (ImageView)findViewById(R.id.a1);
        b = (ImageView)findViewById(R.id.b1);
        v1 = (ImageView)findViewById(R.id.v1);
        g = (ImageView)findViewById(R.id.g1);
        d = (ImageView)findViewById(R.id.d1);
        buttonA = (Button)findViewById(R.id.ba);
        buttonB = (Button)findViewById(R.id.bb);
        buttonV = (Button)findViewById(R.id.bv);
        buttonG = (Button)findViewById(R.id.bg);
        buttonD = (Button)findViewById(R.id.bd);
        ans_string = (TextView)findViewById(R.id.ans_string);
        answer = (TextView)findViewById(R.id.answer);
        cor_string = (TextView)findViewById(R.id.cor_string);
        correct = (TextView)findViewById(R.id.correct);
        prev = (Button)findViewById(R.id.previous);
        stop = (Button)findViewById(R.id.end);
        next = (Button)findViewById(R.id.next);

        cor_string.setVisibility(View.INVISIBLE);

        buttonA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText("А");
                data.get(testNum).setCheck(tam.getAnswers()[0]);
            }
        });

        buttonB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText("Б");
                data.get(testNum).setCheck(tam.getAnswers()[1]);
            }
        });

        buttonV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText("В");
                data.get(testNum).setCheck(tam.getAnswers()[2]);
            }
        });

        buttonG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText("Г");
                data.get(testNum).setCheck(tam.getAnswers()[3]);
            }
        });

        buttonD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer.setText("Д");
                data.get(testNum).setCheck(tam.getAnswers()[4]);
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testNum++;
                if (procesActivity|!procesActivity) setTest(testNum);
                if (endActivity|!endActivity) setRev(testNum);
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testNum--;
                if (procesActivity|!procesActivity) setTest(testNum);
                if (endActivity|!endActivity) setRev(testNum);
            }
        });

        //end test

        buttons = (LinearLayout)findViewById(R.id.linear_button);
        result = (TextView)findViewById(R.id.result);
        review = (Button)findViewById(R.id.review);
        exit = (Button)findViewById(R.id.go_out);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (procesActivity) {
//                    r = false;
                    new MaterialDialog.Builder(TestActivity.this)
                            .title("Увага!")
                            .content("Ви впевнені, що бажаєте завершити тест?")
                            .positiveText("Так")
                            .neutralText("Ні")
                            .titleColor(getResources().getColor(R.color.colorAccent))
                            .backgroundColor(getResources().getColor(R.color.background))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    progressLayout.setVisibility(View.INVISIBLE);
                                    endLayout.setVisibility(View.VISIBLE);
                                    for (TestActivityModel m : data) {
                                        System.out.println("Check:" + m.getCheck() + "\n" +
                                                "Correct:" + m.getCorrect_check());
                                        if (m.getCheck() == m.getCorrect_check()) {
                                            correctCount++;
                                        }
                                    }
                                    procesActivity = false;
                                    endActivity = true;
                                    result.setText("\n" + String.valueOf(correctCount) + "/" + (count + 1));
                                }
                            })
                            .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    procesActivity = true;
                                    endActivity = false;
                                    dialog.cancel();
                                }
                            })
                            .show();

                }
                if(endActivity){
                    seen = true;
                    stop.setVisibility(View.INVISIBLE);
                    review.setVisibility(View.VISIBLE);
                    progressLayout.setVisibility(View.INVISIBLE);
                    endLayout.setVisibility(View.VISIBLE);
                    for (TestActivityModel m : data) {
                        System.out.println("Check:" + m.getCheck() + "\n" +
                                "Correct:" + m.getCorrect_check());
                        if (m.getCheck() == m.getCorrect_check()) {
                            correctCount++;
                        }
                    }
                    result.setText("\n" + String.valueOf(correctCount) + "/" + (count + 1));
                }

            }
        });


        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                procesActivity = false;
                endActivity = true;
                seen = true;
                review.setVisibility(View.INVISIBLE);
                endLayout.setVisibility(View.INVISIBLE);
                progressLayout.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.INVISIBLE);
                cor_string.setVisibility(View.VISIBLE);
                correct.setVisibility(View.VISIBLE);
                stop.setText("Результат");
                stop.setVisibility(View.VISIBLE);
                testNum = 0;
                setRev(testNum);
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(seen) {
                    finish();
                } else {
                    new MaterialDialog.Builder(TestActivity.this)
                            .title("Увага!")
                            .content("Ви впевнені, що бажаєте покинути тест не переглянувши результати?")
                            .titleColor(getResources().getColor(R.color.colorAccent))
                            .positiveText("Так")
                            .neutralText("Ні")
                            .backgroundColor(getResources().getColor(R.color.background))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    finish();
                                }
                            })
                            .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            }
        });

    }

    private void setTest(int n){
        if(testNum == 0){
            prev.setVisibility(View.INVISIBLE);
        } else {
            prev.setVisibility(View.VISIBLE);
        }
        if(testNum == count){
            next.setVisibility(View.INVISIBLE);
        } else {
            next.setVisibility(View.VISIBLE);
        }
        tam = data.get(n);
        number.setText("#"+tam.getNumber());
        test_draw.setImageResource(tam.getTask_test());
        a.setImageResource(tam.getAnswers()[0]);
        b.setImageResource(tam.getAnswers()[1]);
        v1.setImageResource(tam.getAnswers()[2]);
        g.setImageResource(tam.getAnswers()[3]);
        d.setImageResource(tam.getAnswers()[4]);

        if (tam.getCheck() == 0) {
            answer.setText("");
        }

        if (tam.getCheck() == tam.getAnswers()[0]) {
            answer.setText("А");
        }

        if (tam.getCheck() == tam.getAnswers()[1]) {
            answer.setText("Б");
        }

        if (tam.getCheck() == tam.getAnswers()[2]) {
            answer.setText("В");
        }

        if (tam.getCheck() == tam.getAnswers()[3]) {
            answer.setText("Г");
        }

        if (tam.getCheck() == tam.getAnswers()[4]) {
            answer.setText("Д");
        }
    }

    private void setRev(int n){
        if(testNum == 0){
            prev.setVisibility(View.INVISIBLE);
        } else {
            prev.setVisibility(View.VISIBLE);
        }
        if(testNum == count){
            next.setVisibility(View.INVISIBLE);
        } else {
            next.setVisibility(View.VISIBLE);
        }
        tam = data.get(n);
        number.setText("#"+tam.getNumber());
        test_draw.setImageResource(tam.getTask_test());
        a.setImageResource(tam.getAnswers()[0]);
        b.setImageResource(tam.getAnswers()[1]);
        v1.setImageResource(tam.getAnswers()[2]);
        g.setImageResource(tam.getAnswers()[3]);
        d.setImageResource(tam.getAnswers()[4]);

        if (tam.getCheck() == 0) {
            answer.setText("");
        }

        if (tam.getCheck() == tam.getAnswers()[0]) {
            answer.setText("А");
        }

        if (tam.getCheck() == tam.getAnswers()[1]) {
            answer.setText("Б");
        }

        if (tam.getCheck() == tam.getAnswers()[2]) {
            answer.setText("В");
        }

        if (tam.getCheck() == tam.getAnswers()[3]) {
            answer.setText("Г");
        }

        if (tam.getCheck() == tam.getAnswers()[4]) {
            answer.setText("Д");
        }

        if(endActivity) {
            if (tam.getCorrect_check() == tam.getAnswers()[0]) {
                correct.setText("А");
            }

            if (tam.getCorrect_check() == tam.getAnswers()[1]) {
                correct.setText("Б");
            }

            if (tam.getCorrect_check() == tam.getAnswers()[2]) {
                correct.setText("В");
            }

            if (tam.getCorrect_check() == tam.getAnswers()[3]) {
                correct.setText("Г");
            }

            if (tam.getCorrect_check() == tam.getAnswers()[4]) {
                correct.setText("Д");
            }
        }
    }

    public int random(int x,int y){
        int g =(x + (int) (Math.random() * ((y - x) + 1)));
        return g;
    }

    @Override
    public void onBackPressed() {
        if(!startActivity) {
            if (procesActivity) {
                new MaterialDialog.Builder(TestActivity.this)
                        .title("Увага!")
                        .content("Ви впевнені, що бажаєте покинути тест не завершивши його?")
                        .positiveText("Так")
                        .neutralText("Ні")
                        .titleColor(getResources().getColor(R.color.colorAccent))
                        .backgroundColor(getResources().getColor(R.color.background))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                finish();
                            }
                        })
                        .onNeutral(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return;
            }
            if (!seen) {
                new MaterialDialog.Builder(TestActivity.this)
                        .title("Увага!")
                        .content("Ви впевнені, що бажаєте покинути тест не переглянувши результати?")
                        .positiveText("Так")
                        .neutralText("Ні")
                        .titleColor(getResources().getColor(R.color.colorAccent))
                        .backgroundColor(getResources().getColor(R.color.background))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                finish();
                            }
                        })
                        .onNeutral(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return;
            }
        }
//        if(r){
//            super.onBackPressed();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        all = null;
        data = null;
        System.gc();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
