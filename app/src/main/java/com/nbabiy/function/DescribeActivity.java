package com.nbabiy.function;

import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nbabiy.function.model.Function;
import com.nbabiy.function.db.FunctionOpenHelper;

/**
 * Created by Developer on 21.02.2017.
 */

public class DescribeActivity extends AppCompatActivity {

    SQLiteDatabase db;
    Cursor c;
    Cursor cursor;
    int countImage = 0;
    int[] imageId;

    ImageView imageView;
    TextView mTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.describe_function_activity);


        int _id = this.getIntent().getIntExtra("_id", 99999);
        SQLiteOpenHelper oh = new FunctionOpenHelper(DescribeActivity.this);
        db = oh.getReadableDatabase();

        String s = _id + "";

        c = db.rawQuery("Select * from FUNCTION where _id = ?", new String[]{s});

        Function function = new Function();
        c.moveToFirst();
        function.setId(c.getInt(0));
        function.setName(c.getString(1));
        function.setText(c.getString(2));
        function.setIntervalDOfF(c.getString(3));
        function.setIntervalDOfV(c.getString(4));
        function.setParity(c.getString(5));
        function.setNulls(c.getString(6));
        function.setIntervalZS(c.getString(7));
        function.setPeriodicity(c.getString(8));
        function.setMonotony(c.getString(9));
        function.setGraph(c.getString(10));


        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setTitle(function.getName());
        ab.setDisplayHomeAsUpEnabled(true);

        imageView = (ImageView) findViewById(R.id.func_image);
        mTextView = (TextView) findViewById(R.id.page);

        cursor = db.rawQuery("Select * from PHOTO_GRAPHS where text = ?", new String[]{function.getText()});
        cursor.moveToFirst();

        imageId = new int[cursor.getCount()];

        for (int i = 0; i < imageId.length; i++) {
            imageId[i] = cursor.getInt(2);
            cursor.moveToNext();
        }

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.line1);
        if(imageId.length == 1) {
            TextView textView = (TextView)findViewById(R.id.pos);
            linearLayout.removeView(textView);
            linearLayout.removeView(mTextView);
        }else {
            mTextView.setText(countImage + 1 + "/" + imageId.length);
            imageView.setClickable(true);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (countImage < imageId.length - 1) {
                        countImage++;
                    } else {
                        countImage = 0;
                    }
                    imageView.setImageResource(imageId[countImage]);
                    mTextView.setText(countImage + 1 + "/" + imageId.length);
                    System.out.println(countImage);
                }
            });
        }

        imageView.setImageResource(imageId[countImage]);

        TextView text = (TextView) findViewById(R.id.textView);
        text.setText(function.getText());

        LinearLayout linearOVF = (LinearLayout) findViewById(R.id.ovf);
        if (function.getIntervalDOfF().length() > 0) {
            TextView ovf = (TextView) findViewById(R.id.OVF);
            ovf.setText(function.getIntervalDOfF());
        } else {
            linearOVF.removeAllViews();
        }

        LinearLayout linearOZF = (LinearLayout) findViewById(R.id.ozf);
        if (function.getIntervalDOfV().length() > 0) {
            TextView ozf = (TextView) findViewById(R.id.OZF);
            ozf.setText(function.getIntervalDOfV());
        } else {
            linearOZF.removeAllViews();
        }

        LinearLayout linearParity = (LinearLayout) findViewById(R.id.par);
        if (function.getParity().length() > 0) {
            TextView paryti = (TextView) findViewById(R.id.parity);
            paryti.setText(function.getParity());
        } else {
            linearParity.removeAllViews();
        }

        LinearLayout linearNulls = (LinearLayout) findViewById(R.id.nul);
        if (function.getNulls().length() > 0) {
            TextView nulls = (TextView) findViewById(R.id.nulls);
            nulls.setText(function.getNulls());
        } else {
            linearNulls.removeAllViews();
        }

        LinearLayout linearIntervalZS = (LinearLayout) findViewById(R.id.interZS);
        if (function.getIntervalZS().length() > 0) {
            TextView intervalZS = (TextView) findViewById(R.id.intervalZS);
            intervalZS.setText(function.getIntervalZS());
        } else {
            linearIntervalZS.removeAllViews();
        }

        LinearLayout linearPeriodicity = (LinearLayout) findViewById(R.id.perio);
        if (function.getPeriodicity().length() > 0) {
            TextView periodicity = (TextView) findViewById(R.id.periodicity);
            periodicity.setText(function.getPeriodicity());
        } else {
            linearPeriodicity.removeAllViews();
        }

        LinearLayout linearMonotony = (LinearLayout) findViewById(R.id.mono);
        if (function.getMonotony().length() > 0) {
            TextView monotony = (TextView) findViewById(R.id.monotony);
            monotony.setText(function.getMonotony());
        } else {
            linearMonotony.removeAllViews();
        }

        LinearLayout linearGraph = (LinearLayout) findViewById(R.id.graph);
        if (function.getMonotony().length() > 0) {
            TextView monotony = (TextView) findViewById(R.id.graph_text);
            monotony.setText(function.getGraph());
        } else {
            linearGraph.removeAllViews();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        c.close();
        cursor.close();
        db.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
