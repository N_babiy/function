package com.nbabiy.function.model;

import java.util.Arrays;

/**
 * Created by Developer on 27.02.2017.
 */

public class TestActivityModel {
    private int number;
    private int task_test;
    private int [] answers;
    private int check;
    private int correct_check;
    private boolean isCorrect;

    public TestActivityModel() {
    }

    public TestActivityModel(int number, int task_test, int[] answers, int check, int correct_check, boolean isCorrect) {
        this.number = number;
        this.task_test = task_test;
        this.answers = answers;
        this.check = check;
        this.correct_check = correct_check;
        this.isCorrect = isCorrect;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getTask_test() {
        return task_test;
    }

    public void setTask_test(int task_test) {
        this.task_test = task_test;
    }

    public int[] getAnswers() {
        return answers;
    }

    public void setAnswers(int[] answers) {
        this.answers = answers;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public int getCorrect_check() {
        return correct_check;
    }

    public void setCorrect_check(int correct_check) {
        this.correct_check = correct_check;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    @Override
    public String toString() {
        return "TestActivityModel{" +
                "number=" + number +
                ", task_test=" + task_test +
                ", answers=" + Arrays.toString(answers) +
                ", check=" + check +
                ", correct_check=" + correct_check +
                ", isCorrect=" + isCorrect +
                '}';
    }
}
