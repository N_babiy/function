package com.nbabiy.function.model;

/**
 * Created by Developer on 25.02.2017.
 */

public class TestAns {
    private int id;
    private int number;
    private int drawable_ans;

    public TestAns() {
    }

    public TestAns(int id, int number, int drawable_ans) {
        this.id = id;
        this.number = number;
        this.drawable_ans = drawable_ans;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDrawable_ans() {
        return drawable_ans;
    }

    public void setDrawable_ans(int drawable_ans) {
        this.drawable_ans = drawable_ans;
    }
}
