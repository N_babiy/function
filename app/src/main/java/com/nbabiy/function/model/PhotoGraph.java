package com.nbabiy.function.model;

/**
 * Created by Developer on 23.02.2017.
 */

public class PhotoGraph {
    private int _id;
    private String text;
    private int drawableGraph;

    public PhotoGraph() {
    }

    public PhotoGraph(int _id, String text, int drawableGraph) {
        this._id = _id;
        this.text = text;
        this.drawableGraph = drawableGraph;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDrawableGraph() {
        return drawableGraph;
    }

    public void setDrawableGraph(int drawableGraph) {
        this.drawableGraph = drawableGraph;
    }
}
