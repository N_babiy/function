package com.nbabiy.function.model;

/**
 * Created by Developer on 25.02.2017.
 */

public class Test {
    private int id;
    private int number;
    private int drawable_task;
    private int draw_correct_id;
    private String topic;

    public Test() {
    }

    public Test(int id, int number, int drawable_task, int draw_correct_id, String topic) {
        this.id = id;
        this.number = number;
        this.drawable_task = drawable_task;
        this.draw_correct_id = draw_correct_id;
        this.topic = topic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDrawable_task() {
        return drawable_task;
    }

    public void setDrawable_task(int drawable_task) {
        this.drawable_task = drawable_task;
    }

    public int getDraw_correct_id() {
        return draw_correct_id;
    }

    public void setDraw_correct_id(int draw_correct_id) {
        this.draw_correct_id = draw_correct_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
