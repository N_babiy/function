package com.nbabiy.function.model;

/**
 * Created by Developer on 21.02.2017.
 */

public class Function {
    private int id;
    private String name;
    private String text;
    private String intervalDOfF; // OVF
    private String intervalDOfV; // OZF
    private String parity; // parnits
    private String nulls; // nyli
    private String intervalZS; // znakostalist
    private String periodicity; // perioduchnist
    private String monotony; // monotonnist
    private String graph;

    public Function() {

    }

    public Function(int id, String name, String text, String intervalDOfF, String intervalDOfV, String parity, String nulls, String intervalZS, String periodicity, String monotony, String graph) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.intervalDOfF = intervalDOfF;
        this.intervalDOfV = intervalDOfV;
        this.parity = parity;
        this.nulls = nulls;
        this.intervalZS = intervalZS;
        this.periodicity = periodicity;
        this.monotony = monotony;
        this.graph = graph;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIntervalDOfF() {
        return intervalDOfF;
    }

    public void setIntervalDOfF(String intervalDOfF) {
        this.intervalDOfF = intervalDOfF;
    }

    public String getIntervalDOfV() {
        return intervalDOfV;
    }

    public void setIntervalDOfV(String intervalDOfV) {
        this.intervalDOfV = intervalDOfV;
    }

    public String getParity() {
        return parity;
    }

    public void setParity(String parity) {
        this.parity = parity;
    }

    public String getNulls() {
        return nulls;
    }

    public void setNulls(String nulls) {
        this.nulls = nulls;
    }

    public String getIntervalZS() {
        return intervalZS;
    }

    public void setIntervalZS(String intervalZS) {
        this.intervalZS = intervalZS;
    }

    public String getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(String periodicity) {
        this.periodicity = periodicity;
    }

    public String getMonotony() {
        return monotony;
    }

    public void setMonotony(String monotony) {
        this.monotony = monotony;
    }

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }
}
