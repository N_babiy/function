package com.nbabiy.function.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nbabiy.function.R;
import com.nbabiy.function.model.Function;
import com.nbabiy.function.model.PhotoGraph;
import com.nbabiy.function.model.Test;
import com.nbabiy.function.model.TestAns;


/**
 * Created by Developer on 21.02.2017.
 */

public class FunctionOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "function"; // the name of our database
    private static final String T_NAME = "FUNCTION";
    private static final int DB_VERSION = 1; // the version of the database

    public FunctionOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE "+ T_NAME + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "name TEXT,"
                    + "text TEXT, "
                    + "intervalDOfF TEXT, "
                    + "intervalDOfV TEXT, "
                    + "parity TEXT, "
                    + "nulls TEXT, "
                    + "intervalZS TEXT, "
                    + "periodicity TEXT, "
                    + "monotony TEXT,"
                    + "graph TEXT);");
            db.execSQL("CREATE TABLE PHOTO_GRAPHS (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "text TEXT, " +
                    "draw INTEGER);");

            db.execSQL("CREATE TABLE TEST (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "number INTEGER, " +
                    "drawable_task INTEGER, " +
                    "draw_correct_id INTEGER, " +
                    "topic TEXT);");
            db.execSQL("CREATE TABLE TEST_ANS (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "number INTEGER, " +
                    "drawable_ans INTEGER);");

            db.execSQL("CREATE TABLE MARKS (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "mark TEXT, " +
                    "value TEXT);");

            insertTest(db, new Test(0,1000,R.drawable.t1000,R.drawable.v1000,"Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1000, R.drawable.a1000));
            insertTestAns(db, new TestAns(0, 1000, R.drawable.b1000));
            insertTestAns(db, new TestAns(0, 1000, R.drawable.v1000));
            insertTestAns(db, new TestAns(0, 1000, R.drawable.g1000));
            insertTestAns(db, new TestAns(0, 1000, R.drawable.d1000));

            insertTest(db, new Test(0, 1001, R.drawable.t1001, R.drawable.g1001, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1001, R.drawable.a1001));
            insertTestAns(db, new TestAns(0, 1001, R.drawable.b1001));
            insertTestAns(db, new TestAns(0, 1001, R.drawable.v1001));
            insertTestAns(db, new TestAns(0, 1001, R.drawable.g1001));
            insertTestAns(db, new TestAns(0, 1001, R.drawable.d1001));

            insertTest(db, new Test(0, 1002, R.drawable.t1002, R.drawable.v1002, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1002, R.drawable.a1002));
            insertTestAns(db, new TestAns(0, 1002, R.drawable.b1002));
            insertTestAns(db, new TestAns(0, 1002, R.drawable.v1002));
            insertTestAns(db, new TestAns(0, 1002, R.drawable.g1002));
            insertTestAns(db, new TestAns(0, 1002, R.drawable.d1002));

            insertTest(db, new Test(0, 1003, R.drawable.t1003, R.drawable.b1003, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1003, R.drawable.a1003));
            insertTestAns(db, new TestAns(0, 1003, R.drawable.b1003));
            insertTestAns(db, new TestAns(0, 1003, R.drawable.v1003));
            insertTestAns(db, new TestAns(0, 1003, R.drawable.g1003));
            insertTestAns(db, new TestAns(0, 1003, R.drawable.d1003));

            insertTest(db, new Test(0, 1004, R.drawable.t1004, R.drawable.a1004, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1004, R.drawable.a1004));
            insertTestAns(db, new TestAns(0, 1004, R.drawable.b1004));
            insertTestAns(db, new TestAns(0, 1004, R.drawable.v1004));
            insertTestAns(db, new TestAns(0, 1004, R.drawable.g1004));
            insertTestAns(db, new TestAns(0, 1004, R.drawable.d1004));

            insertTest(db, new Test(0, 1005, R.drawable.t1005, R.drawable.g1005, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1005, R.drawable.a1005));
            insertTestAns(db, new TestAns(0, 1005, R.drawable.b1005));
            insertTestAns(db, new TestAns(0, 1005, R.drawable.v1005));
            insertTestAns(db, new TestAns(0, 1005, R.drawable.g1005));
            insertTestAns(db, new TestAns(0, 1005, R.drawable.d1005));

            insertTest(db, new Test(0, 1006, R.drawable.t1006, R.drawable.a1006, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1006, R.drawable.a1006));
            insertTestAns(db, new TestAns(0, 1006, R.drawable.b1006));
            insertTestAns(db, new TestAns(0, 1006, R.drawable.v1006));
            insertTestAns(db, new TestAns(0, 1006, R.drawable.g1006));
            insertTestAns(db, new TestAns(0, 1006, R.drawable.d1006));

            insertTest(db, new Test(0, 1007, R.drawable.t1007, R.drawable.g1007, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1007, R.drawable.a1007));
            insertTestAns(db, new TestAns(0, 1007, R.drawable.b1007));
            insertTestAns(db, new TestAns(0, 1007, R.drawable.v1007));
            insertTestAns(db, new TestAns(0, 1007, R.drawable.g1007));
            insertTestAns(db, new TestAns(0, 1007, R.drawable.d1007));

            insertTest(db, new Test(0, 1008, R.drawable.t1008, R.drawable.d1008, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1008, R.drawable.a1008));
            insertTestAns(db, new TestAns(0, 1008, R.drawable.b1008));
            insertTestAns(db, new TestAns(0, 1008, R.drawable.v1008));
            insertTestAns(db, new TestAns(0, 1008, R.drawable.g1008));
            insertTestAns(db, new TestAns(0, 1008, R.drawable.d1008));

            insertTest(db, new Test(0, 1009, R.drawable.t1009, R.drawable.a1009, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1009, R.drawable.a1009));
            insertTestAns(db, new TestAns(0, 1009, R.drawable.b1009));
            insertTestAns(db, new TestAns(0, 1009, R.drawable.v1009));
            insertTestAns(db, new TestAns(0, 1009, R.drawable.g1009));
            insertTestAns(db, new TestAns(0, 1009, R.drawable.d1009));

            insertTest(db, new Test(0, 1010, R.drawable.t1010, R.drawable.d1010, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1010, R.drawable.b1010));
            insertTestAns(db, new TestAns(0, 1010, R.drawable.v1010));
            insertTestAns(db, new TestAns(0, 1010, R.drawable.g1010));
            insertTestAns(db, new TestAns(0, 1010, R.drawable.a1010));
            insertTestAns(db, new TestAns(0, 1010, R.drawable.d1010));

            insertTest(db, new Test(0, 1011, R.drawable.t1011, R.drawable.a1011, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1011, R.drawable.a1011));
            insertTestAns(db, new TestAns(0, 1011, R.drawable.b1011));
            insertTestAns(db, new TestAns(0, 1011, R.drawable.v1011));
            insertTestAns(db, new TestAns(0, 1011, R.drawable.g1011));
            insertTestAns(db, new TestAns(0, 1011, R.drawable.d1011));

            insertTest(db, new Test(0, 1012, R.drawable.t1012, R.drawable.b1012, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1012, R.drawable.a1012));
            insertTestAns(db, new TestAns(0, 1012, R.drawable.b1012));
            insertTestAns(db, new TestAns(0, 1012, R.drawable.v1012));
            insertTestAns(db, new TestAns(0, 1012, R.drawable.g1012));
            insertTestAns(db, new TestAns(0, 1012, R.drawable.d1012));

            insertTest(db, new Test(0, 1013, R.drawable.t1013, R.drawable.b1013, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1013, R.drawable.a1013));
            insertTestAns(db, new TestAns(0, 1013, R.drawable.b1013));
            insertTestAns(db, new TestAns(0, 1013, R.drawable.v1013));
            insertTestAns(db, new TestAns(0, 1013, R.drawable.g1013));
            insertTestAns(db, new TestAns(0, 1013, R.drawable.d1013));

            insertTest(db, new Test(0, 1014, R.drawable.t1014, R.drawable.v1014, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1014, R.drawable.a1014));
            insertTestAns(db, new TestAns(0, 1014, R.drawable.b1014));
            insertTestAns(db, new TestAns(0, 1014, R.drawable.v1014));
            insertTestAns(db, new TestAns(0, 1014, R.drawable.g1014));
            insertTestAns(db, new TestAns(0, 1014, R.drawable.d1014));

            insertTest(db, new Test(0, 1015, R.drawable.t1015, R.drawable.d1015, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1015, R.drawable.a1015));
            insertTestAns(db, new TestAns(0, 1015, R.drawable.b1015));
            insertTestAns(db, new TestAns(0, 1015, R.drawable.v1015));
            insertTestAns(db, new TestAns(0, 1015, R.drawable.g1015));
            insertTestAns(db, new TestAns(0, 1015, R.drawable.d1015));

            insertTest(db, new Test(0, 1016, R.drawable.t1016, R.drawable.g1016, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1016, R.drawable.a1016));
            insertTestAns(db, new TestAns(0, 1016, R.drawable.b1016));
            insertTestAns(db, new TestAns(0, 1016, R.drawable.v1016));
            insertTestAns(db, new TestAns(0, 1016, R.drawable.g1016));
            insertTestAns(db, new TestAns(0, 1016, R.drawable.d1016));

            insertTest(db, new Test(0, 1017, R.drawable.t1017, R.drawable.g1017, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1017, R.drawable.a1017));
            insertTestAns(db, new TestAns(0, 1017, R.drawable.b1017));
            insertTestAns(db, new TestAns(0, 1017, R.drawable.v1017));
            insertTestAns(db, new TestAns(0, 1017, R.drawable.g1017));
            insertTestAns(db, new TestAns(0, 1017, R.drawable.d1017));

            insertTest(db, new Test(0, 1018, R.drawable.t1018, R.drawable.b1018, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1018, R.drawable.a1018));
            insertTestAns(db, new TestAns(0, 1018, R.drawable.b1018));
            insertTestAns(db, new TestAns(0, 1018, R.drawable.v1018));
            insertTestAns(db, new TestAns(0, 1018, R.drawable.g1018));
            insertTestAns(db, new TestAns(0, 1018, R.drawable.d1018));

            insertTest(db, new Test(0, 1019, R.drawable.t1019, R.drawable.v1019, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1019, R.drawable.a1019));
            insertTestAns(db, new TestAns(0, 1019, R.drawable.b1019));
            insertTestAns(db, new TestAns(0, 1019, R.drawable.v1019));
            insertTestAns(db, new TestAns(0, 1019, R.drawable.g1019));
            insertTestAns(db, new TestAns(0, 1019, R.drawable.d1019));

            insertTest(db, new Test(0, 1020, R.drawable.t1020, R.drawable.d1020, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1020, R.drawable.a1020));
            insertTestAns(db, new TestAns(0, 1020, R.drawable.b1020));
            insertTestAns(db, new TestAns(0, 1020, R.drawable.v1020));
            insertTestAns(db, new TestAns(0, 1020, R.drawable.g1020));
            insertTestAns(db, new TestAns(0, 1020, R.drawable.d1020));

            insertTest(db, new Test(0, 1021, R.drawable.t1021, R.drawable.g1021, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1021, R.drawable.a1021));
            insertTestAns(db, new TestAns(0, 1021, R.drawable.b1021));
            insertTestAns(db, new TestAns(0, 1021, R.drawable.v1021));
            insertTestAns(db, new TestAns(0, 1021, R.drawable.g1021));
            insertTestAns(db, new TestAns(0, 1021, R.drawable.d1021));

            insertTest(db, new Test(0, 1022, R.drawable.t1022, R.drawable.v1022, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1022, R.drawable.a1022));
            insertTestAns(db, new TestAns(0, 1022, R.drawable.b1022));
            insertTestAns(db, new TestAns(0, 1022, R.drawable.v1022));
            insertTestAns(db, new TestAns(0, 1022, R.drawable.g1022));
            insertTestAns(db, new TestAns(0, 1022, R.drawable.d1022));

            insertTest(db, new Test(0, 1023, R.drawable.t1023, R.drawable.b1023, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1023, R.drawable.a1023));
            insertTestAns(db, new TestAns(0, 1023, R.drawable.b1023));
            insertTestAns(db, new TestAns(0, 1023, R.drawable.v1023));
            insertTestAns(db, new TestAns(0, 1023, R.drawable.g1023));
            insertTestAns(db, new TestAns(0, 1023, R.drawable.d1023));

            insertTest(db, new Test(0, 1024, R.drawable.t1024, R.drawable.a1024, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1024, R.drawable.a1024));
            insertTestAns(db, new TestAns(0, 1024, R.drawable.b1024));
            insertTestAns(db, new TestAns(0, 1024, R.drawable.v1024));
            insertTestAns(db, new TestAns(0, 1024, R.drawable.g1024));
            insertTestAns(db, new TestAns(0, 1024, R.drawable.d1024));

            insertTest(db, new Test(0, 1025, R.drawable.t1025, R.drawable.g1025, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1025, R.drawable.a1025));
            insertTestAns(db, new TestAns(0, 1025, R.drawable.b1025));
            insertTestAns(db, new TestAns(0, 1025, R.drawable.v1025));
            insertTestAns(db, new TestAns(0, 1025, R.drawable.g1025));
            insertTestAns(db, new TestAns(0, 1025, R.drawable.d1025));

            insertTest(db, new Test(0, 1026, R.drawable.t1026, R.drawable.v1026, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1026, R.drawable.a1026));
            insertTestAns(db, new TestAns(0, 1026, R.drawable.b1026));
            insertTestAns(db, new TestAns(0, 1026, R.drawable.v1026));
            insertTestAns(db, new TestAns(0, 1026, R.drawable.g1026));
            insertTestAns(db, new TestAns(0, 1026, R.drawable.d1026));

            insertTest(db, new Test(0, 1027, R.drawable.t1027, R.drawable.d1027, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1027, R.drawable.a1027));
            insertTestAns(db, new TestAns(0, 1027, R.drawable.b1027));
            insertTestAns(db, new TestAns(0, 1027, R.drawable.v1027));
            insertTestAns(db, new TestAns(0, 1027, R.drawable.g1027));
            insertTestAns(db, new TestAns(0, 1027, R.drawable.d1027));

            insertTest(db, new Test(0, 1028, R.drawable.t1028, R.drawable.d1028, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1028, R.drawable.a1028));
            insertTestAns(db, new TestAns(0, 1028, R.drawable.b1028));
            insertTestAns(db, new TestAns(0, 1028, R.drawable.v1028));
            insertTestAns(db, new TestAns(0, 1028, R.drawable.g1028));
            insertTestAns(db, new TestAns(0, 1028, R.drawable.d1028));

            insertTest(db, new Test(0, 1029, R.drawable.t1029, R.drawable.b1029, "Загальний Тест"));
            insertTestAns(db, new TestAns(0, 1029, R.drawable.a1029));
            insertTestAns(db, new TestAns(0, 1029, R.drawable.b1029));
            insertTestAns(db, new TestAns(0, 1029, R.drawable.v1029));
            insertTestAns(db, new TestAns(0, 1029, R.drawable.g1029));
            insertTestAns(db, new TestAns(0, 1029, R.drawable.d1029));



            insertFunctions(db, new Function(0,
                    "Пряма пропорційність",
                    "y = kx",
                    "R",
                    "R",
                    "Непарна",
                    "якщо x=0, то y=0",
                    "",
                    "",
                    "Якщо k>0, то функція зростає на множині R\n" +
                            "Якщо k<0, то функція спадає на множині R",
                    "Графіком прямої пропорційності є пряма, яка проходить через початок координат"));

            insertPhotoGraph(db, new PhotoGraph(0,"y = kx",R.drawable.draw_graph_y_kx_1));
            insertPhotoGraph(db, new PhotoGraph(0,"y = kx",R.drawable.draw_graph_y_kx_2));

            insertFunctions(db, new Function(0,
                    "Лінійна функція",
                    "y = kx + b (k є R, b є R)",
                    "R",
                    "R, якщо k≠0\n" +
                            "{b}, якщо k=0",
                    "Якщо k≠0, b≠0, то функція ні парна, ні непарна\n" +
                            "Якщо k=0 - функція парна\n" +
                            "Якщо b=0, k≠0 - непарна\n" +
                            "Якщо k=0, b=0 - і парна, і непарна",
                    "Якщо x=0, то y=b\n" +
                            "Якщо x = - b/k, функція y=0",
                    "",
                    "",
                    "Якщо k>0, то функція зростає на множині R\n" +
                            "Якщо k<0, то функція спадає на множині R\n" +
                            "Якщо k=0, то функція постійна, y=b",
                    "Графіком лінійної функції є пряма, що утворює з віссю абсцис кут φ, тангенс якого дорівнює k"));
            insertPhotoGraph(db, new PhotoGraph(0,"y = kx + b (k є R, b є R)", R.drawable.draw_graph_y_kx_b_1));
            insertPhotoGraph(db, new PhotoGraph(0,"y = kx + b (k є R, b є R)", R.drawable.draw_graph_y_kx_b_2));
            insertPhotoGraph(db, new PhotoGraph(0,"y = kx + b (k є R, b є R)", R.drawable.draw_graph_y_kx_b_3));

            insertFunctions(db, new Function(0,
                    "Обернена пропорційність",
                    "y = k/x (k є R, k ≠ 0)",
                    "(-∞;0)∪(0;+∞)",
                    "(-∞;0)∪(0;+∞)",
                    "Непарна",
                    "Графік функції y = k/x вісь координат не перетинає",
                    "Якщо k>0, то при x>0 функція набуває додатних значить, а при x<0 - від'ємних\n" +
                            "Якщо k<0, то при x>0 функція набуває від'ємних значить, а при x<0 - додатних",
                    "",
                    "Якщо k>0, то на інтервалі (-∞;0) функція спадає, на інтервалі (0;+∞) також спадає\n" +
                            "Якщо k<0, то на інтервалах (-∞;0),(0;+∞) функція зростає",
                    "Графіком оберненої пропорційності є гіпербола"));
            insertPhotoGraph(db, new PhotoGraph(0,"y = k/x (k є R, k ≠ 0)",R.drawable.draw_graph_y_k_x_1));
            insertPhotoGraph(db, new PhotoGraph(0,"y = k/x (k є R, k ≠ 0)",R.drawable.draw_graph_y_k_x_2));

            insertFunctions(db, new Function(0,
                    "y = |x|",
                    "y = |x|",
                    "R",
                    "[0;+∞)",
                    "Парна",
                    "Якщо x=0, то y=0\n" +
                            "Якщо x≠0, то y > 0\n" +
                            "Графік функції розташований у верхній координатній півплощині",
                    "",
                    "",
                    "Якщо x<0, то функція спадає\n" +
                            "Якщо x>0, то функція зростає",
                    "Графіком функції є об'єдання двох променів:бісектрис першої та другої координатних чвертей"));
            insertPhotoGraph(db, new PhotoGraph(0,"y = |x|",R.drawable.draw_graph_y__x__1));

            insertFunctions(db, new Function(0,
                    "Дробово-лінійна функція",
                    "y = (ax+b)/(cx+d)",
                    "(-∞;-d/c)∪(-d/c;+∞)",
                    "(-∞;a/c)∪(a/c;+∞)",
                    "",
                    "точки (-b/a;0) i (0;b/d)",
                    "",
                    "",
                    "Функція зростає на кожному з проміжків (-∞;-d/c),(-d/c;+∞), якщо ad>bc\n" +
                            "Функція спадає на кожному з проміжків (-∞;-d/c),(-d/c;+∞), якщо ad<bc",
                    "Графіком функції є гіпербола"));
            insertPhotoGraph(db,new PhotoGraph(0,"y = (ax+b)/(cx+d)",R.drawable.draw_graph_y_axb_cxd_1));
            insertPhotoGraph(db,new PhotoGraph(0,"y = (ax+b)/(cx+d)",R.drawable.draw_graph_y_axb_cxd_2));

            insertFunctions(db, new Function(0,
                    "Квадратична функція",
                    "y = ax^2 + bx + c",
                    "R",
                    "Якщо а>0, то [-(b^2-4ac)/4a;+∞)\n" +
                            "Якщо a<0, то (-∞;-(b^2-4ac)/4a]",
                    "Якщо b≠0, то функція ні парна, ні непарна\n" +
                            "Якщо b=0, то функція y = ax^2 +c парна",
                    "Графік функції перетинає осі координат у точках (0;c),(x1;0),(x2;0), де\n x1=x2=(-b±sqrt(b^2-4ac)/2a ",
                    "",
                    "",
                    "Якщо а<0, то функція спадає на проміжку (-∞;-b/2a] і зростає на проміжку [-b/2a;+∞),\n х0=-b/2a - точка мінімуму\n" +
                            "Якщо а>0, то функція зростає на проміжку (-∞;-b/2a] і спадає на проміжку [-b/2a;+∞),\n х0=-b/2a - точка максимуму\n",
                    "Графіком функції є парабола, вітки якої напрямлені вгору, якщо а>0, і вниз, якщо а<0\n" +
                            "координати вершини \n ( -b/2a;- ((b^2-4ac)/4a) )\n" +
                            "вісь симетрії графіка \n x = -b/2a"));

            insertPhotoGraph(db, new PhotoGraph(0, "y = ax^2 + bx + c",R.drawable.draw_graph_y_ax2_bx_c_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = ax^2 + bx + c",R.drawable.draw_graph_y_ax2_bx_c_2));

            insertFunctions(db, new Function(0,
                    "Степенева функція",
                    "y = x^n (n є N)",
                    "R",
                    "Якщо n=2k, k є N, то [0;+∞)\n" +
                            "Якщо n=2k-1, k є N, то (-∞;+∞)",
                    "Якщо n=2k, k є N, то функція парна\n" +
                            "Якщо n=2k-1, k є N, то функція непарна",
                    "",
                    "",
                    "",
                    "Якщо n=2k, k є N, то функція спадає на проміжку (-∞;0] і зростає на проміжку [0;+∞)\n" +
                            "Якщо n=2k-1, k є N, то функція зростає на множині R",
                    "Графік функції проходить через початок координат\n" +
                            "Графік функції симетричний відносно осі Оу, n=2k, k є N\n" +
                            "Відносно початку координат - якщо n=2k-1, k є N"));

            insertPhotoGraph(db, new PhotoGraph(0, "y = x^n (n є N)", R.drawable.draw_graph_y_x_n_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^n (n є N)", R.drawable.draw_graph_y_x_n_2));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^n (n є N)", R.drawable.draw_graph_y_x_n_3));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^n (n є N)", R.drawable.draw_graph_y_x_n_4));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^n (n є N)", R.drawable.draw_graph_y_x_n_5));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^n (n є N)", R.drawable.draw_graph_y_x_n_6));

            insertFunctions(db, new Function(0,
                    "Степенева функція",
                    "y = x^-n (n є N)",
                    "(-∞;0)∪(0;+∞)",
                    "Якщо n=2k, k є N, то [0;+∞)\n" +
                            "Якщо n=2k-1, k є N, то (-∞0)∪(0;+∞)",
                    "Якщо n=2k, k є N, то функція парна, графік симетричний відносно осі Оу\n" +
                            "Якщо n=2k-1, k є N, то функція непарна і графік симетричний відносно початку координат",
                    "Точок перетину з осями координат графік функції не має",
                    "",
                    "",
                    "Якщо n=2k, k є N, то функція зростає на проміжку (-∞;0) і спадає на проміжку (0;+∞)\n" +
                            "Якщо n=2k-1, k є N, то функція спадає на кожному з проміжків (-∞;0),(0;+∞)",
                    ""));

            insertPhotoGraph(db, new PhotoGraph(0, "y = x^-n (n є N)", R.drawable.draw_graph_y_x__n_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^-n (n є N)", R.drawable.draw_graph_y_x__n_2));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^-n (n є N)", R.drawable.draw_graph_y_x__n_3));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^-n (n є N)", R.drawable.draw_graph_y_x__n_4));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^-n (n є N)", R.drawable.draw_graph_y_x__n_5));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^-n (n є N)", R.drawable.draw_graph_y_x__n_6));

            insertFunctions(db, new Function(0,
                    "Степенева функція",
                    "y = x^a (a - неціле число)",
                    "[0;+∞), якщо a>0\n(0;+∞), якщо a<0",
                    "[0;+∞), якщо a>0\n" +
                            "(0;+∞), якщо a<0",
                    "Функція ні парна, ні непарна",
                    "",
                    "",
                    "",
                    "Якщо a>0, то функція зростає на всій області визначення\n" +
                            "Якщо a<0, то функція спадає на всій області визначення",
                    "Якщо a>0, то графік функції проходить через початок координат\n" +
                            "Якщо a<0, то графік функції не перетинає осей координат"));

            insertPhotoGraph(db, new PhotoGraph(0, "y = x^a (a - неціле число)", R.drawable.draw_graph_y_x_a_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^a (a - неціле число)", R.drawable.draw_graph_y_x_a_2));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^a (a - неціле число)", R.drawable.draw_graph_y_x_a_3));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^a (a - неціле число)", R.drawable.draw_graph_y_x_a_4));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^a (a - неціле число)", R.drawable.draw_graph_y_x_a_5));
            insertPhotoGraph(db, new PhotoGraph(0, "y = x^a (a - неціле число)", R.drawable.draw_graph_y_x_a_6));

            insertFunctions(db, new Function(0,
                    "y = exp(In(x)/n), де n>=2, n є N",
                    "y = exp(In(x)/n), де n>=2, n є N",
                    "Якщо n = 2k, k є N, то [0;+∞)\n" +
                            "Якщо n = 2k+1, k є N, то (-∞;+∞)",
                    "Якщо n = 2k, k є N, то [0;+∞)\n" +
                            "Якщо n = 2k+1, k є N, то (-∞;+∞)",
                    "Якщо n = 2k, k є N, то функція ні парна, ні непарна\n" +
                            "Якщо n = 2k+1, k є N, то функція непарна і її графік симетричний відносно початку координат ",
                    "",
                    "",
                    "",
                    "Функція зростає на всій області визначення",
                    "Графік функції проходить через початок координат"));

            insertPhotoGraph(db, new PhotoGraph(0, "y = exp(In(x)/n), де n>=2, n є N", R.drawable.draw_graph_y_n_sqrt_x_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = exp(In(x)/n), де n>=2, n є N", R.drawable.draw_graph_y_n_sqrt_x_2));
            insertPhotoGraph(db, new PhotoGraph(0, "y = exp(In(x)/n), де n>=2, n є N", R.drawable.draw_graph_y_n_sqrt_x_3));
            insertPhotoGraph(db, new PhotoGraph(0, "y = exp(In(x)/n), де n>=2, n є N", R.drawable.draw_graph_y_n_sqrt_x_4));

            insertFunctions(db, new Function(0,
                    "Показникова функція",
                    "y = a^x, де a>0, a≠1",
                    "R",
                    "(0;+∞)",
                    "Функція ні парна, ні непарна",
                    "",
                    "",
                    "",
                    "Якщо a>0, то функція зростає на множині R\n" +
                            "Якщо 0<a<1, то функція спадає на множині R",
                    "Графік функції перетинає вісь Оу в точці (0;1), вісь Ох не перетинає"));

            insertPhotoGraph(db, new PhotoGraph(0, "y = a^x, де a>0, a≠1", R.drawable.draw_graph_y_ax_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = a^x, де a>0, a≠1", R.drawable.draw_graph_y_ax_2));

            insertFunctions(db, new Function(0,
                    "Логарифмічна функція",
                    "y = log(a)x, де a>0, a≠1",
                    "(0;+∞)",
                    "R",
                    "Функція ні парна, ні непарна",
                    "",
                    "",
                    "",
                    "Якщо a>1, то функція зростає на всій області визначення)\n" +
                            "Якщо 0<a<1, то функція спадає на всій області визначення",
                    "Графік функції перетинає вісь Оx в точці (1;0), вісь Оy не перетинає"));

            insertPhotoGraph(db, new PhotoGraph(0, "y = log(a)x, де a>0, a≠1", R.drawable.draw_graph_y_log_a_x_1));
            insertPhotoGraph(db, new PhotoGraph(0, "y = log(a)x, де a>0, a≠1", R.drawable.draw_graph_y_log_a_x_2));

            insertFunctions(db, new Function(0,
                    "y=[x]",
                    "y=[x]",
                    "R",
                    "Область значень Z\n Якщо 0<=x<1, то y=[x]=0\n" +
                            "Якщо -1<=x<0, то y=[x]=-1 і т. д.",
                    "Функція ні парна, ні непарна\n" +
                            "На кожному з проміжків [n;n+1), n є Z, функція постійна",
                    "",
                    "",
                    "",
                    "",
                    ""));

            insertPhotoGraph(db, new PhotoGraph(0, "y=[x]", R.drawable.draw_graph_y_x___));

            insertFunctions(db, new Function(0,
                    "y={x}=x-[x]",
                    "y={x}=x-[x]",
                    "R",
                    "[0;1)",
                    "Функція ні парна, ні непарна",
                    "",
                    "",
                    "Функція періодична, її період дорівнює 1",
                    "На кожно з проміжків [n;n+1), n є Z функція зростає",
                    ""));

            insertPhotoGraph(db, new PhotoGraph(0, "y={x}=x-[x]", R.drawable.draw_graph_y_x_x_x));

            insertFunctions(db, new Function(0,
                    "y=sin x",
                    "y=sin x",
                    "R",
                    "[-1;1]",
                    "Непарна",
                    "πk, k є Z",
                    "y>0, якщо x є (2πk; π+2πk),k є Z\n" +
                            "y<0, якщо x є (π+2πk; 2π+2πk),k є Z",
                    "Найменший додатний період 2π",
                    "Функція зростає на кожному з проміжків\n[-π/2+2πk; π/2+2πk],k є Z\nі спадає на кожному з проміжків\n[π/2+2πk; 3π/2+2πk],k є Z",
                    "Функція набуває найбільшого значення y max = 1 у точках\nx max = π/2+2πk,k є Z\nі найменшого значення y min = -1 у точках\nx min = -π/2+2πk,k є Z\nГрафіком функції y=sin x є синусоїда"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=sin x",R.drawable.draw_graph_y_sin_x));

            insertFunctions(db, new Function(0,
                    "y=cos x",
                    "y=cos x",
                    "R",
                    "[-1;1]",
                    "Парна",
                    "π/2+πk, k є Z",
                    "y>0, якщо x є (-π/2+2πk; π/2+2πk),k є Z\n" +
                            "y<0, якщо x є (π/2+2πk; 3π/2+2πk),k є Z",
                    "Найменший додатний період 2π",
                    "Функція зростає на кожному з проміжків\n" +
                            "[-π+2πk; 2πk],k є Z\n" +
                            "і спадає на кожному з проміжків\n" +
                            "[2πk; π+2πk],k є Z",
                    "Функція набуває найбільшого значення y max = 1 у точках\n" +
                            "x max = 2πk,k є Z\n" +
                            "і найменшого значення y min = -1 у точках\n" +
                            "x min = π+2πk,k є Z\n" +
                            "Графіком функції y=cos x є косинусоїда"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=cos x",R.drawable.draw_graph_y_cos_x));

            insertFunctions(db, new Function(0,
                    "y=tg x",
                    "y=tg x",
                    "x≠π/2+πk,k є Z",
                    "R",
                    "Непарна",
                    "πk",
                    "y>0, якщо x є (πk; π/2+πk),k є Z\n" +
                            "y<0, якщо x є (-π/2+πk; πk),k є Z",
                    "Найменший додатний період π",
                    "Функція зростає на кожному з проміжків\n" +
                            "(-π/2+πk; π/2+πk),k є Z",
                    "Найменших і найбільших значень функція не має\n" +
                            "Графіком функції y=tg x є тангенсоїда"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=tg x", R.drawable.draw_graph_y_tg_x));

            insertFunctions(db, new Function(0,
                    "y=ctg x",
                    "y=ctg x",
                    "x≠πk,k є Z",
                    "R",
                    "Непарна",
                    "π/2+πk, k є Z",
                    "y>0, якщо x є (πk; π/2+πk),k є Z\n" +
                            "y<0, якщо x є (π/2+πk; π+πk),k є Z",
                    "Найменший додатний період π",
                    "Функція спадає на кожному з проміжків\n" +
                            "(πk; π+πk),k є Z",
                    "Найменших і найбільших значень функція не має\n" +
                            "Графіком функції y=ctg x є котангенсоїда"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=ctg x",R.drawable.draw_graph_y_ctg_x));

            insertFunctions(db, new Function(0,
                    "y=arcsin x",
                    "y=arcsin x",
                    "[-1;1]",
                    "[-π/2;π/2]",
                    "Непарна",
                    "x=0",
                    "y>0, якщо x є (0;1]\n" +
                            "y<0, якщо x є [-1;0)",
                    "",
                    "Функція зростає при x є [-1;1]",
                    "Функція набуває найбільшого значення y max = π/2 у точці\n" +
                            "x max = 1\n" +
                            "і найменшого значення y min = -π/2 у точці\n" +
                            "x min = -1\n" +
                            "Графік функції перетинає осі координат у точці (0;0)"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=arcsin x", R.drawable.draw_graph_y_arcsin_x));

            insertFunctions(db, new Function(0,
                    "y=arccos x",
                    "y=arccos x",
                    "[-1;1]",
                    "[0;π]",
                    "Функція ні парна, ні непарна\n" +
                            "arccos(-x)=π-arccos x",
                    "x=1",
                    "y>0, якщо x є [-1;1]",
                    "",
                    "Функція спадає при x є [-1;1]",
                    "Функція набуває найбільшого значення y max = π у точці\n" +
                            "x max = -1\n" +
                            "і найменшого значення y min = 0 у точці\n" +
                            "x min = 1\n" +
                            "Графік функції перетинає вісь Оу у точці (0; π/2)"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=arccos x", R.drawable.draw_graph_y_arccos_x));

            insertFunctions(db, new Function(0,
                    "y=arctg x",
                    "y=arctg x",
                    "R",
                    "(-π/2; π/2)",
                    "Непарна",
                    "x=0",
                    "y>0, якщо x є (0;+∞)\n" +
                            "y<0, якщо x є (-∞;0)",
                    "",
                    "Функція зростає при x є [-1;1]",
                    "Найменших і найбільших значень функція не має\n" +
                            "Графік функції перетинає осі координат у точці (0;0)"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=arctg x", R.drawable.draw_graph_y_arctg_x));

            insertFunctions(db, new Function(0,
                    "y=arcctg x",
                    "y=arcctg x",
                    "R",
                    "(0;π)",
                    "Функція ні парна, ні непарна\n" +
                            "arcctg(-x)=π-arcctg x",
                    "",
                    "y>0, якщо x є R",
                    "",
                    "Функція спадає при x є R",
                    "Найменших і найбільших значень функція не має\n" +
                            "Графік функції перетинає вісь Оу у точці (0;π/2)б а вісь Ох не перетинає"));

            insertPhotoGraph(db, new PhotoGraph(0, "y=arcctg x", R.drawable.draw_graph_y_arcctg_x));

            insertMark(db, "sqrt(x)", "Корінь квадратний з числа х");
            insertMark(db, "x^a", "Піднесення до степеня а числа х");
            insertMark(db, "exp(In(x)/n)", "Корінь n-го степеня з числа х");
            insertMark(db, "log(a)x", "Логарифм числа х за основою а");
            insertMark(db, "Використані матеріали",
                    "\n1. О.С.Істер\nМатематика ДОВІДНИК + ТЕСТИ\nВидавництво \"Абетка\" 2016" +
                    "\n2. В.А.Вишенський М.О.Перестюк А.М.Самойленко\nЗбірник задач з математики \nидавництво \"Либідь\" 1990" +
                            "\n3. Математика Комплексний довідник\nВидавництво \"Весна\" 2014 " +
                            "\n4. О.М.Роганін В.А.Дергачов С.М.Малярчук\nАлгебра, геометрія та інформатика в таблицях\nВидавництво \"Книжковий клуб\" 2006");
        }

    }

    private static void insertFunctions(SQLiteDatabase db, Function function) {
        ContentValues drinkValues = new ContentValues();
        drinkValues.put("name",function.getName());
        drinkValues.put("text", function.getText());
        drinkValues.put("intervalDOfF", function.getIntervalDOfF());
        drinkValues.put("intervalDOfV", function.getIntervalDOfV());
        drinkValues.put("parity", function.getParity());
        drinkValues.put("nulls", function.getNulls());
        drinkValues.put("intervalZS", function.getIntervalZS());
        drinkValues.put("periodicity", function.getPeriodicity());
        drinkValues.put("monotony", function.getMonotony());
        drinkValues.put("graph",function.getGraph());
        db.insert(T_NAME, null, drinkValues);
    }

    private static void insertPhotoGraph(SQLiteDatabase db, PhotoGraph photoGraph){
        ContentValues contentValues = new ContentValues();
        contentValues.put("text",photoGraph.getText());
        contentValues.put("draw",photoGraph.getDrawableGraph());
        db.insert("PHOTO_GRAPHS", null, contentValues);
    }

    private static void insertTest(SQLiteDatabase db, Test test) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("number", test.getNumber());
        contentValues.put("drawable_task", test.getDrawable_task());
        contentValues.put("draw_correct_id", test.getDraw_correct_id());
        contentValues.put("topic", test.getTopic());
        db.insert("TEST", null, contentValues);
    }

    private static void insertTestAns(SQLiteDatabase db, TestAns testAns) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("number", testAns.getNumber());
        contentValues.put("drawable_ans", testAns.getDrawable_ans());
        db.insert("TEST_ANS", null, contentValues);
    }

    public static void insertMark(SQLiteDatabase db, String mark, String value) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("mark", mark);
        contentValues.put("value", value);
        db.insert("MARKS", null, contentValues);
    }
}
