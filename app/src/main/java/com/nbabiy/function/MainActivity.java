package com.nbabiy.function;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nbabiy.function.db.FunctionOpenHelper;
import com.nbabiy.function.model.TestActivityModel;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor cursor;
    private Cursor c;
    int number = 0;

    boolean click = false;
    ListView list_options;
    ListView list_second;
    ListView list_third;
    ListView list_fourth;
    AdapterView.OnItemClickListener itemClickListenerOptions;
    AdapterView.OnItemClickListener itemClickListenerSecond;
    AdapterView.OnItemClickListener itemClickListenerThird;
    android.support.v7.app.ActionBar ab;

    int [] marray = {
            R.drawable.m1001,
            R.drawable.m1002,
            R.drawable.m1003,
            R.drawable.m1004,
            R.drawable.m1005,
            R.drawable.m1006,
            R.drawable.m1007,
            R.drawable.m1008,
            R.drawable.m1009,
            R.drawable.m1010,
            R.drawable.m1011,
            R.drawable.m1012,
            R.drawable.m1013,
            R.drawable.m1014,
            R.drawable.m1015,
            R.drawable.m1016,
    };

    int [] iarray = {
            R.drawable.i1001,
            R.drawable.i1002,
            R.drawable.i1003,
            R.drawable.i1004,
            R.drawable.i1005,
            R.drawable.i1006,
            R.drawable.i1007,
            R.drawable.i1008,
            R.drawable.i1009,
            R.drawable.i1010,
            R.drawable.i1011,
            R.drawable.i1012,
            R.drawable.i1013,
            R.drawable.i1014,
            R.drawable.i1015,
            R.drawable.i1016,
            R.drawable.i1017,
            R.drawable.i1018,
            R.drawable.i1019,
            R.drawable.i1020,
            R.drawable.i1021,
            R.drawable.i1022,
            R.drawable.i1023,
            R.drawable.i1024,
            R.drawable.i1025,
            R.drawable.i1026,
            R.drawable.i1027,
            R.drawable.i1028,
            R.drawable.i1029,
            R.drawable.i1030,
            R.drawable.i1031,
            R.drawable.i1032,
            R.drawable.i1033,
            R.drawable.i1034,
            R.drawable.i1035,
            R.drawable.i1036,
            R.drawable.i1037,
            R.drawable.i1038,
            R.drawable.i1039,
            R.drawable.i1040,
            R.drawable.i1041,
            R.drawable.i1042,
            R.drawable.i1043,
            R.drawable.i1044,
            R.drawable.i1045,
            R.drawable.i1046,
            R.drawable.i1047,
            R.drawable.i1048,
            R.drawable.i1049,
            R.drawable.i1050,
            R.drawable.i1051,
            R.drawable.i1052,
            R.drawable.i1053,
            R.drawable.i1054,
            R.drawable.i1055,
            R.drawable.i1056,
            R.drawable.i1057,
            R.drawable.i1058,
            R.drawable.i1059,
            R.drawable.i1060,
            R.drawable.i1061,
            R.drawable.i1062
    };

    int [] rarray = {
            R.drawable.r1001,
            R.drawable.r1002,
            R.drawable.r1003,
            R.drawable.r1004,
            R.drawable.r1005,
            R.drawable.r1006,
            R.drawable.r1007,
            R.drawable.r1008,
            R.drawable.r1009,
            R.drawable.r1010,
            R.drawable.r1011,
            R.drawable.r1012,
            R.drawable.r1013,
            R.drawable.r1014,
            R.drawable.r1015,
            R.drawable.r1016,
            R.drawable.r1017,
            R.drawable.r1018,
            R.drawable.r1019
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ab = getSupportActionBar();

        list_options = (ListView) findViewById(R.id.list_options);
        list_second = (ListView) findViewById(R.id.list_second);
        list_third = (ListView) findViewById(R.id.list_third);
        list_fourth = (ListView) findViewById(R.id.list_fourth);

        list_fourth.setClickable(false);

        list_second.setVisibility(View.INVISIBLE);
        list_third.setVisibility(View.INVISIBLE);
        list_fourth.setVisibility(View.INVISIBLE);

        //ClickListener for list_options
        itemClickListenerOptions = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        Intent in = new Intent(MainActivity.this, TestActivity.class);
                        startActivity(in);
                        break;
                    case 1:
                        list_options.setVisibility(View.INVISIBLE);
                        list_second.setVisibility(View.VISIBLE);
                        click = true;
                        number = 1;
                        ab.setDisplayHomeAsUpEnabled(true);
                        ab.setTitle(R.string.mat);
                        break;
                    case 2:
                        SQLiteOpenHelper openHelper = new FunctionOpenHelper(getApplicationContext());
                        db = openHelper.getReadableDatabase();

                        cursor = db.query("FUNCTION",
                                new String[]{"_id","text"},
                                null, null, null, null, null);

                        CursorAdapter listAdapter = new SimpleCursorAdapter(getApplicationContext(),
                                android.R.layout.simple_list_item_1,
                                cursor,
                                new String[]{"text"},
                                new int[]{android.R.id.text1},
                                0);
                        list_third.setAdapter(listAdapter);
                        list_options.setVisibility(View.INVISIBLE);
                        list_third.setVisibility(View.VISIBLE);
                        click = true;
                        number = 2;
                        ab.setDisplayHomeAsUpEnabled(true);
                        ab.setTitle(R.string.vlast);
                        break;
                    case 3:
                        click = true;
                        number = 3;


                        SQLiteOpenHelper openHelper1 = new FunctionOpenHelper(getApplicationContext());
                        db = openHelper1.getReadableDatabase();

                        c = db.rawQuery("SELECT * FROM MARKS", null);

                        CursorAdapter listAdapter1 = new SimpleCursorAdapter(getApplicationContext(),
                                android.R.layout.simple_list_item_2,
                                c,
                                new String[]{"mark", "value"},
                                new int[]{android.R.id.text1, android.R.id.text2},
                                0);

                        list_fourth.setAdapter(listAdapter1);

                        ab.setDisplayHomeAsUpEnabled(true);
                        ab.setTitle(R.string.marks);

                        list_options.setVisibility(View.INVISIBLE);
                        list_fourth.setVisibility(View.VISIBLE);
                        break;
                }
            }
        };
        //set OnItemClickListener for list_options
        list_options.setOnItemClickListener(itemClickListenerOptions);

        //ClickListener for list_second
        itemClickListenerSecond = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        Intent intent = new Intent(MainActivity.this, FunctionDoc.class);
                        Bundle b = new Bundle();
                        b.putIntArray("array", marray);
                        intent.putExtras(b);
                        intent.putExtra("title",getResources().getString(R.string.ZZVM));
                        startActivity(intent);
                        break;
                    case 1:
                        Intent intent1 = new Intent(MainActivity.this, FunctionDoc.class);
                        Bundle b1 = new Bundle();
                        b1.putIntArray("array", iarray);
                        intent1.putExtras(b1);
                        intent1.putExtra("title", getResources().getString(R.string.MDT));
                        startActivity(intent1);
                        break;
                    case 2:
                        Intent intent2 = new Intent(MainActivity.this, FunctionDoc.class);
                        intent2.putExtra("title", getResources().getString(R.string.MKD));
                        Bundle b2 = new Bundle();
                        b2.putIntArray("array", rarray);
                        intent2.putExtras(b2);
                        startActivity(intent2);
                        break;
                }
            }
        };
        //set OnItemClickListener for list_second
        list_second.setOnItemClickListener(itemClickListenerSecond);

        //ClickListener for list_third
        itemClickListenerThird = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor a123 = ((SimpleCursorAdapter)parent.getAdapter()).getCursor();
                a123.moveToPosition(position);
                int _id= a123.getInt(0);
                Intent intent = new Intent(MainActivity.this, DescribeActivity.class);
                intent.putExtra("_id", _id);
                startActivity(intent);
            }
        };
        //set OnItemClickListener for list_third
        list_third.setOnItemClickListener(itemClickListenerThird);


    }

    @Override
    public void onBackPressed() {
        if(click == true) {
            switch (number) {
                case 1:
                    list_second.setVisibility(View.INVISIBLE);
                    list_options.setVisibility(View.VISIBLE);
                    click = false;
                    break;
                case 2:
                    list_third.setVisibility(View.INVISIBLE);
                    list_options.setVisibility(View.VISIBLE);
                    click = false;
                    break;
                case 3:
                    list_fourth.setVisibility(View.INVISIBLE);
                    list_options.setVisibility(View.VISIBLE);
                    click = false;
            }
            number = 0;
            ab.setDisplayHomeAsUpEnabled(false);
            ab.setTitle(R.string.app_name);
        } else {
            new MaterialDialog.Builder(MainActivity.this)
                    .title("Увага!")
                    .content("Ви впевнені, що бажаєте вийти?")
                    .positiveText("Так")
                    .neutralText("Ні")
                    .titleColor(getResources().getColor(R.color.colorAccent))
                    .backgroundColor(getResources().getColor(R.color.background))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            finish();
                        }
                    })
                    .onNeutral(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.cancel();
                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(click == true) {
                    switch (number) {
                        case 1:
                            list_second.setVisibility(View.INVISIBLE);
                            list_options.setVisibility(View.VISIBLE);
                            click = false;
                            break;
                        case 2:
                            list_third.setVisibility(View.INVISIBLE);
                            list_options.setVisibility(View.VISIBLE);
                            click = false;
                            break;
                        case 3:
                            list_fourth.setVisibility(View.INVISIBLE);
                            list_options.setVisibility(View.VISIBLE);
                            click = false;
                    }
                    number = 0;
                }
                ab.setDisplayHomeAsUpEnabled(false);
                ab.setTitle(R.string.app_name);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
        cursor.close();
        c.close();
    }
}
